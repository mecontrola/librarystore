﻿namespace LibraryStore.Tests.Mocks.Datas
{
    public static class UserData
    {
        public const string Id = "34e82311-1ef5-4332-af5c-9af5d8c135ae";
        public const string Fullname = "Administrator";
        public const string Username = "admin";
        public const string CreatedAt = "2020-01-01";
        public const bool Active = true;
    }
}