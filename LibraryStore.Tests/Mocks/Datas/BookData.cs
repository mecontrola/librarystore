﻿namespace LibraryStore.Tests.Mocks.Datas
{
    public static class BookData
    {
        public const string Id = "131ed5cd-49d8-4ab8-9db2-b00743d781b3";
        public const string Title = "Book Tittle";
        public const string Description = "Book description and more information";
        public const string Author = "Book writer";
        public const string CreatedAt = "2020-01-01";
        public const bool Active = true;
    }
}