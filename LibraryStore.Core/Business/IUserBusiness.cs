﻿using LibraryStore.Core.Data.Dtos;

namespace LibraryStore.Core.Business
{
    public interface IUserBusiness : IBusiness<UserDto, UserInputDto>
    { }
}