﻿using LibraryStore.Core.Data.Dtos;

namespace LibraryStore.Core.Business
{
    public interface IBookBusiness : IBusiness<BookDto, BookInputDto>
    { }
}